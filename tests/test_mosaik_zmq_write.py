import json
import os
import time

import pytest
import zmq

import mosaik_components.zmq.writer as mosaik_zmq_write
from tests.ZMQReceiver import ZMQReceiver

DATA_FILE = os.path.join(os.path.dirname(__file__), "data.json")


def test_init_create():
    sim = mosaik_zmq_write.Simulator()
    meta = sim.init("WriteSim", 1.0, step_size=15 * 60)
    assert meta["models"] == {
        "Socket": {
            "public": True,
            "any_inputs": True,
            "params": ["hosts", "ports", "socket_types", "socket_modes"],
            "attrs": ["sync"],
        }
    }
    entities = sim.create(
        1, "Socket", hosts=["tcp://127.0.0.1"], ports=[5558], socket_types=["PUB"]
    )

    assert entities == [{"eid": "ZMQWrite_0", "type": "Socket"}]

    entities = sim.create(
        2,
        "Socket",
        hosts=["tcp://127.0.0.1", "tcp://127.0.0.1"],
        ports=[5559, 5560],
        socket_types=["PUB", "PUSH"],
        socket_modes=["bind", "connect"],
    )

    assert entities == [
        {"eid": "ZMQWrite_%s" % i, "type": "Socket"} for i in range(0, 2)
    ]


def test_init_create_errors():
    sim = mosaik_zmq_write.Simulator()
    sim.init("WriteSim", 1.0, step_size=15 * 60)

    # Invalid socket type
    with pytest.raises(AssertionError):
        sim.create(
            1,
            "Socket",
            hosts=["tcp://127.0.0.1"],
            ports=[5558],
            socket_types=["SUB"],
        )

    # Invalid model name
    with pytest.raises(ValueError):
        sim.create(
            1, "model", hosts=["tcp://127.0.0.1"], ports=[5558], socket_types=["PUB"]
        )

    # Duplicate port
    with pytest.raises(zmq.error.ZMQError):
        sim.create(
            2,
            "Socket",
            hosts=["tcp://127.0.0.1", "tcp://127.0.0.1"],
            ports=[5558, 5558],
            socket_types=["PUB", "PUB"],
            socket_modes=["bind", "bind"],
        )


def test_step():
    host = "tcp://127.0.0.1"
    port = 5561
    duration = 2

    write_sim = mosaik_zmq_write.Simulator()
    write_sim.init("WriteSim", 1.0, step_size=15)
    write_socket = write_sim.create(
        1, "Socket", hosts=[host], ports=[port], socket_types=["PUB"]
    )
    socket_eid = write_socket[0]["eid"]

    client = ZMQReceiver(
        host="tcp://127.0.0.1",
        port=port,
        rec_file_path=DATA_FILE,
        duration=duration,
    )
    client.start()

    # Wait because otherwise ZeroMQs pub-sub-mechanism may lose first messages
    time.sleep(0.3)

    data = [
        {socket_eid: {"val_out": {"Sim-0.0.0": 1, "Sim-1.0.1": 1, "Sim-1.0.0": 1}}},
        {socket_eid: {"val_out": {"Sim-0.0.0": 2, "Sim-1.0.1": 2, "Sim-1.0.0": 2}}},
    ]

    # Emulate Steps
    ret = write_sim.step(0, {}, 100)
    assert ret == write_sim.step_size
    write_sim.step(15, data[0], 10)
    write_sim.step(30, data[1], 10)
    client.join()

    # Check results

    # Check number of lines
    file_result_num_lines = sum(1 for _ in open(DATA_FILE, "r"))
    assert (
        len(data) == file_result_num_lines
    ), "Number of sent and received messages is not the same."

    # Check content line by line
    with open(DATA_FILE, "r") as openfile:
        for line in data:
            rec_line = openfile.readline()
            json1 = json.loads(rec_line)
            json1 = json.dumps(json1, sort_keys=True)
            json2 = json.dumps(line[socket_eid], sort_keys=True)
            assert json1 == json2, "Sent and received messages are not the same."

    os.remove(DATA_FILE)
