import zmq
import threading


class ZMQReceiver(threading.Thread):
    def __init__(self, host, port, rec_file_path, duration):
        self.host = host
        self.port = port
        self.recFilePath = rec_file_path
        self.duration = duration
        threading.Thread.__init__(self)

    def run(self):
        context = zmq.Context()
        receiver = context.socket(zmq.SUB)
        receiver.bind(self.host + ":" + str(self.port))
        receiver.setsockopt_string(zmq.SUBSCRIBE, "")
        receiver.RCVTIMEO = 1000  # timeout in ms

        result = ""
        for i in range(self.duration):
            message = receiver.recv_string()
            topic, content = message.split(maxsplit=1)
            result = result + content + "\n"

        with open(self.recFilePath, "w+") as file:
            file.write(result)

        receiver.close()
        context.term()
