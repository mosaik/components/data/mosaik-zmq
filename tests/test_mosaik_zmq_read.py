import os

import pytest

import mosaik_components.zmq.reader as mosaik_zmq_read
from tests.ZMQSender import ZMQSender

DATA_FILE = os.path.join(os.path.dirname(__file__), "data.json")


def test_init_create():
    sim = mosaik_zmq_read.Simulator()
    meta = sim.init("ReadSim", 1.0, attrs=["val_in"])
    assert meta["models"] == {
        "Socket": {
            "public": True,
            "any_inputs": True,
            "params": [
                "host",
                "port",
                "socket_mode",
                "topicfilter",
                "split_topic",
                "return_topic",
            ],
            "attrs": ["val_in"],
        }
    }
    entities = sim.create(1, "Socket", host="tcp://127.0.0.1", port=5558)

    assert entities == [{"eid": "ZMQRead", "type": "Socket"}]


def test_init_create_errors():
    sim = mosaik_zmq_read.Simulator()

    # Missing attrs
    with pytest.raises(ValueError):
        sim.init("Socket", 1.0, attrs=[])

    meta = sim.init("ReadSim", 1.0, attrs=["val_in"])

    # Invalid socket number
    with pytest.raises(ValueError):
        sim.create(2, "Socket", host="tcp://127.0.0.1", port=5558)

    # Invalid model name
    with pytest.raises(ValueError):
        sim.create(1, "model", host="tcp://127.0.0.1", port=5558)

def test_step():
    host = "tcp://127.0.0.1"
    port = 5561

    read_sim = mosaik_zmq_read.Simulator()
    read_sim.init("ReadSim", 1.0, ["val_in"], step_size=15)
    read_socket = read_sim.create(1, "Socket", host=host, port=port)
    socket_eid = read_socket[0]["eid"]

    data = [
        {"val_in": {"Sim-0.0.0": 1, "Sim-1.0.1": 1, "Sim-1.0.0": 1}},
        {"val_in": {"Sim-0.0.0": 2, "Sim-1.0.1": 2, "Sim-1.0.0": 2}},
    ]

    client = ZMQSender(host="tcp://127.0.0.1", port=port, filepath=DATA_FILE, data=data)
    client.start()

    # Emulate Steps
    ret = read_sim.step(0, {}, 100)
    assert ret == read_sim.step_size
    assert data[0] == read_sim.get_data({socket_eid: ["val_in"]})[socket_eid]
    read_sim.step(15, {}, 10)
    assert data[1] == read_sim.get_data({socket_eid: ["val_in"]})[socket_eid]
    client.join()
