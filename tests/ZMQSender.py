import threading

import zmq
import time


class ZMQSender(threading.Thread):
    sender = None
    line: int = 0

    def __init__(self, host, port, filepath, data):
        self.host = host
        self.port = port
        self.filepath = filepath
        self.data = data
        threading.Thread.__init__(self)

    def run(self):
        context = zmq.Context()
        self.sender = context.socket(zmq.PUB)
        self.sender.connect(self.host + ":" + str(self.port))

        # wait a little so that the receiver has time to wait for the first message
        time.sleep(1)
        for line in self.data:
            self.sender.send_json(line)
