# mosaik-zmq-write
Sends mosaik simulation data to ZeroMQ sockets.

## Installation

TBD

## Usage

```
SIM_CONFIG = {
    "zmq-write": {
        "python": "mosaik_components.zmq.writer:Simulator",
    },
}
```

## Starting the simulator

When you start mosaik_zmq_write, you can provide a *step_size* argument that defines how often data will be sent.

```
write_sim = world.start('ZMQWrite', step_size=5)
```

## Creating the sockets

Every instance of mosaik_zmq_write allows you to create instances of its *Socket* model. The *Socket* has the following parameters:

- *hosts* is the address of the host the data will be sent to.
- *port*s is the port on which the host will receive data.
- *socket_types* is the type of the ZeroMQ socket to open for connection between the adapter and the host. Two different patterns can be used. The default is '
  PUB', which stands for the [Publish-Subscribe pattern](https://zguide.zeromq.org/docs/chapter1/#Getting-the-Message-Out). Alternatively
  the [Push-Pull pattern](https://zguide.zeromq.org/docs/chapter1/#Divide-and-Conquer) ('PUSH') can be used.
- *socket_modes* defines how the socket connects to the address. It can be either "bind" or "connect". 
            Bind can only be done once on an address, connects can be done multiple times. At least one participant needs to bind. Defaults to ["bind"].

Each parameter has to be specified as a list.

```
write_sockets = zmq_write.Socket(hosts=["tcp://127.0.0.1"], ports=[5558], socket_types = ["PUB"], socket_modes = ["connect"])
```

## Connecting the sockets
To get data sent to the host, the zmq model has to be connected to another model in the scenario definition. Based on
the [mosaik-demo](https://gitlab.com/mosaik/examples/mosaik-demo) the scenario could look like this:

An example is given in the `example_send.py` scenario. Here, the publiser *connects* to the address, while the subsriber *binds*. In this example, it has to be 
ensured that the subscriber starts first, to that the address is already bound when the publisher connects. The subsriber also needs to do the first step to 
already wait for messages on the socket before the publisher writes them.

# mosaik-zmq-read
Receives data from a ZeroMQ socket (e.g. from another mosaik simulator). This simulator uses a 'SUB' socket from the
[Publish-Subscribe pattern](https://zguide.zeromq.org/docs/chapter1/#Getting-the-Message-Out). 

## Installation

TBD

## Usage

```
SIM_CONFIG = {
    "zmq-read": {
        "python": "mosaik_components.zmq.reader:Simulator",
    }
}
```

## Starting the simulator
When you start zmq-read there are a few parameters you can provide.

- **attrs** is a list of the attributes that will be received from the clients
- *step_size* (default: 1) how often the data will be received

```
zmq_read = world.start("zmq-read", attrs=["P"], step_size=60)
```

## Creating the sockets
Every instance of mosaik_zmq_read allows you to create 1 instance of its *Socket* model. The *Socket* has the following parameters:

- *host* is the address of the host the data will be sent from.
- *port* is the port on which the socket will receive data.
- *socket_mode* (_SOCKET_MODE_TYPES, optional): Socket mode of zeroMQ. Can be either "bind" or "connect".
            Bind can only be done once on an address, connects can be done multiple times. At least one participant needs to bind. Defaults to "bind".
- *topicfilter* (str, optional): If set, the socket filters the zeroMQ messages by the topic (see zeroMQ documentation for details on topics). Defaults to "".
- *split_topic* (bool, optional): If set to true, the incomind messages are split by .split(maxsplit=1) to separate the topic from the message body. Defaults to False.
- *return_topic* (bool, optional): If set to true, the topic and body of the message are included in the returned data element in the form {"topic": topic, "message": message}.
            If set to false, only the pure message is returned. Defaults to False.

Each instance of the simulator only allows for one socket and any additional calls will override the previously created socket.

```
read_socket = zmq_read.Socket(
    host="tcp://127.0.0.1",
    port=5558,
    return_topic=True,
    split_topic=True,
    socket_mode="bind",
)
```

## Connecting the sockets
To get data sent from the host to the simulation the zmq model has to be connected to another model in the scenario definition. Based on
the [mosaik-demo](https://gitlab.com/mosaik/examples/mosaik-demo) the scenario could look like this:

See `example_receive.py` for an example. 

# Authors
The mosaik ZeroMQ connector was initially created by Jan Sören Schwarz. The current version is mainly developed by Lennart Bruns and updated by Tobias Brandt.

# Issues
This adapter does not fit your needs for using it with mosaik? Feel free to create an issue so that we can discuss on how to improve this adapter.

# Changelog

## 1.0
- Complete rewrite with option to read and write

## 0.1.1 – 2016-01-20
- Updated documentation

## 0.1 – 2015-12-16
- Initial release