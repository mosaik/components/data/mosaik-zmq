"""
Receives mosaik simulation data from a ZeroMQ socket.
"""
import mosaik_api
import zmq
from loguru import logger
import json
from typing import Literal, get_args

_SOCKET_MODE_TYPES = Literal["bind", "connect"]

META = {
    "type": "time-based",
    "models": {
        "Socket": {
            "public": True,
            "any_inputs": True,
            "params": [
                "host",
                "port",
                "socket_mode",
                "topicfilter",
                "split_topic",
                "return_topic",
            ],
            "attrs": [],
        }
    },
}


class Simulator(mosaik_api.Simulator):
    socket: zmq.Socket
    host: str
    port: int
    return_topic: bool
    split_tpic: bool

    def __init__(self):
        super().__init__(META)
        self.context = zmq.Context()
        self.wait_for_input = True
        self.data = {}
        self.eid = "ZMQRead"
        self.timeout = None
        self.step_size = None

    def init(self, sid, time_resolution, attrs, step_size=1):
        if len(attrs) == 0:
            raise ValueError("List of attributes expected. Got: %s" % attrs)
        self.meta["models"]["Socket"]["attrs"] = attrs
        self.step_size = step_size
        return self.meta

    def create(
        self,
        num: int,
        model: str,
        host: str,
        port: int,
        socket_mode: _SOCKET_MODE_TYPES = "bind",
        topicfilter: str = "",
        split_topic: bool = False,
        return_topic: bool = False,
    ):
        """Creates and initializes the sockets.

        Args:
            num (int): Number of sockets, needs to be 1.
            model (str): Model name, needs to be "Socket".
            host (str): Name of the host, e.g. "127.0.0.1"
            port (int): Name of the port, e.g. 5558.
            socket_mode (_SOCKET_MODE_TYPES, optional): Socket mode of zeroMQ. Can be either "bind" or "connect".
            Bind can only be done once on an address, connects can be done multiple times. At least one participant needs to bind. Defaults to "bind".
            topicfilter (str, optional): If set, the socket filters the zeroMQ messages by the topic (see zeroMQ documentation for details on topics). Defaults to "".
            split_topic (bool, optional): If set to true, the incomind messages are split by .split(maxsplit=1) to separate the topic from the message body. Defaults to False.
            return_topic (bool, optional): If set to true, the topic and body of the message are included in the returned data element in the form {"topic": topic, "message": message}.
            If set to false, only the pure message is returned. Defaults to False.
        """
        if num != 1:
            raise ValueError("Only one socket can be created.")
        if model != "Socket":
            raise ValueError("Unknown model: %s" % model)
        self.return_topic = return_topic

        # Assert that the socket mode is correct
        socket_mode_options = get_args(_SOCKET_MODE_TYPES)
        assert (
            socket_mode in socket_mode_options
        ), f"'{socket_mode}' is not in {socket_mode_options}"

        self.socket = self.context.socket(zmq.SUB)
        self.split_tpic = split_topic

        # If we bind, we need to be the first on this port, if we connect, someone else has to bind (e.g., the publisher)
        address: str = host + ":" + str(port)
        if socket_mode == "bind":
            self.socket.bind(address)
        elif socket_mode == "connect":
            self.socket.connect(address)

        self.socket.setsockopt_string(zmq.SUBSCRIBE, topicfilter)
        return [{"eid": self.eid, "type": model}]

    def receive_inputs(self, time):
        """Receive json data from the socket and send a json response with the given timestamp"""
        data = self.socket.recv()
        if self.split_tpic:
            topic, messagedata = data.split(maxsplit=1)
            topic = topic.decode("utf-8")
        else:
            messagedata = data
            topic = ""

        message_string = messagedata.decode("utf-8")
        message = json.loads(message_string)

        if self.return_topic:
            return_data = {"topic": topic, "message": message}
            return return_data
        else:
            return message

    def step(self, time, inputs, max_advance):
        self.data = self.receive_inputs(time)
        return time + self.step_size

    def get_data(self, outputs):
        data = {}
        if len(self.data) > 0:
            data[self.eid] = self.data
        return data

    def finalize(self):
        if hasattr(self, "socket"):
            self.socket.unbind(self.socket.LAST_ENDPOINT)
        else:
            logger.warning("Socket shall be unbinded but was never created.")


def main():
    desc = __doc__.strip()
    return mosaik_api.start_simulation(Simulator(), desc)


if __name__ == "__main__":
    main()
