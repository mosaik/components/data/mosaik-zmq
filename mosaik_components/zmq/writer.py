"""
Sends mosaik simulation data to ZeroMQ sockets.
"""

import mosaik_api
import zmq
import json
from typing import Literal, get_args, List

_SOCKET_MODE_TYPES = Literal["bind", "connect"]
_SOCKET_TYPES = Literal["PUSH", "PUB"]

META = {
    "type": "time-based",
    "models": {
        "Socket": {
            "public": True,
            "any_inputs": True,
            "params": ["hosts", "ports", "socket_types", "socket_modes"],
            "attrs": ["sync"],
        }
    },
}


class Simulator(mosaik_api.Simulator):
    def __init__(self):
        super().__init__(META)
        self.step_size = None
        self.sockets = {}
        self.context = zmq.Context()

    def init(self, sid, time_resolution, step_size=1):
        self.step_size = step_size
        return self.meta

    def create(
        self,
        num: int,
        model: str,
        hosts: List[str],
        ports: List[int],
        socket_types: List[_SOCKET_TYPES] = ["PUB"],
        socket_modes: List[_SOCKET_MODE_TYPES] = ["connect"],
    ):
        """Creates and initializes the sockets.

        Args:
            num (int): Number of writer sockets
            model (str): Name of the model, has to be "Socket"
            hosts (List[str]): List of the hosts, e.g. "127.0.0.1"
            ports (List[int]): List of the ports, e.g. 5558
            socket_types (List[_SOCKET_TYPES], optional): List of the socket types o zeroMQ. Can be PUSH, PUB, or REQ. Defaults to ["PUB"].
            socket_modes (List[_SOCKET_MODE_TYPES], optional): List of the socket modes. Can be either "bind" or "connect".
            Bind can only be done once on an address, connects can be done multiple times. At least one participant needs to bind. Defaults to ["bind"].
        """
        if model != "Socket":
            raise ValueError("Unknown model: %s" % model)

        # Assert that the socket type is correct
        socket_type_options = get_args(_SOCKET_TYPES)
        for socket_type in socket_types:
            assert (
                socket_type in socket_type_options
            ), f"'{socket_type}' is not in {socket_type_options}"

        # Assert that the socket mode is correct
        socket_mode_options = get_args(_SOCKET_MODE_TYPES)
        for socket_mode in socket_modes:
            assert (
                socket_mode in socket_mode_options
            ), f"'{socket_mode}' is not in {socket_mode_options}"

        sockets = []

        # Configure the sockets
        for model_number in range(num):
            eid = self._create_socket(
                hosts, ports, socket_types, socket_modes, model_number
            )
            sockets.append({"eid": eid, "type": model})

        return sockets

    def _create_socket(self, hosts, ports, socket_types, socket_modes, model_number):
        eid = "ZMQWrite_%s" % (model_number)
        if socket_types[model_number] == "PUSH":
            self.sockets[eid] = self.context.socket(zmq.PUSH)
        elif socket_types[model_number] == "PUB":
            self.sockets[eid] = self.context.socket(zmq.PUB)
        else:
            raise ValueError(
                "Socket type %s not supported. Allowed are PUSH and PUB"
                % socket_types[model_number]
            )
        address: str = hosts[model_number] + ":" + str(ports[model_number])
        if socket_modes[model_number] == "bind":
            self.sockets[eid].bind(address)
        else:
            self.sockets[eid].connect(address)
        return eid

    def step(self, time, inputs, max_advance):
        """Sends the input it receives directly to the socket.
        Should be overriden if data should be processed beforehand."""
        for eid, socket in self.sockets.items():
            if eid in inputs:
                inputs[eid].update({"timestamp": time})
                topic = str(eid)
                inputs_as_json = json.dumps(inputs[eid])
                socket.send_string("%s %s" % (topic, inputs_as_json))
        return time + self.step_size

    def get_data(self, outputs):
        return {}

    def finalize(self):
        for socket in self.sockets.values():
            socket.unbind(socket.LAST_ENDPOINT)


def main():
    desc = __doc__.strip()
    return mosaik_api.start_simulation(Simulator(), desc)


if __name__ == "__main__":
    main()
