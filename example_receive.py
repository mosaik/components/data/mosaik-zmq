# A simple example to use the zeroMQ adapter to connect two mosaik simulations. Start the receiver first and then the publisher / sender.


import mosaik

sim_config = {
    "zmq-read": {
        "python": "mosaik_components.zmq.reader:Simulator",
    },
    "Collector": {
        "cmd": "%(python)s collector.py %(addr)s",
    },
}

world = mosaik.World(sim_config, mosaik_config={"addr": ("127.0.0.1", 5555)})
collector = world.start("Collector")

# Connect CSV input and monitor to test if it works at all
monitor = collector.Monitor()

# Specify the zmq reader with its socket to read from. For the attributes, you have two options in this example:
# 1. return_topic=True and use attrs=["topic", "message"] -> You receive the messages and the topics
# 2. return_topic=False and use attrs=["P"] -> You only get the content of "P"
zmq_read = world.start("zmq-read", attrs=["topic", "message"], step_size=60)

# Start the subscriber, subscribe to the address and bind it, so that others (the publisher) can connect to this address.
# We want to have the topic (the entity id, eid) in the result, so we have to split it from the content and return it in the result.
read_socket = zmq_read.Socket(
    host="tcp://127.0.0.1",
    port=5558,
    return_topic=True,
    split_topic=True,
    socket_mode="bind",
)

world.connect(read_socket, monitor, "topic", "message")

END = 4 * 60
world.run(until=END)
