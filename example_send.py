# A simple example to use the zeroMQ adapter to connect two mosaik simulations. Start the receiver first and then the publisher / sender.

import mosaik
import mosaik_components.zmq.reader
import mosaik_csv

sim_config = {
    "zmq-write": {
        "python": "mosaik_components.zmq.writer:Simulator",
    },
    "CSV": {
        "python": "mosaik_csv:CSV",
    },
}

world = mosaik.World(sim_config, mosaik_config={"addr": ("127.0.0.1", 5556)})
csv_sim = world.start(
    "CSV",
    sim_start="2014-01-01 00:01:00",
    datafile="test_data.csv",
    date_format="YYYY-MM-DD HH:mm:ss",
    delimiter=",",
)
csv = csv_sim.CSV.create(1)

# Specify the zmq writer with its socket to write to
zmq_write = world.start("zmq-write", step_size=60)
# We use the pub-type to publish messages in a pub-sub-scheme. The socket mode is "connect", so that we connect to the address
# that the subsriber has already used "bind" on.
write_socket = zmq_write.Socket(
    hosts=["tcp://127.0.0.1"],
    ports=[5558],
    socket_types=["PUB"],
    socket_modes=["connect"],
)

world.connect(csv[0], write_socket, "P")

END = 4 * 60
world.run(until=END)
